﻿using Domain;
using Microsoft.AspNetCore.Mvc;
using Service;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TestAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WardController : ControllerBase
    {
        private readonly IWardService WardService;
        public WardController(IWardService wardService)
        {
            this.WardService = wardService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllWards()
        {
            var wards = await this.WardService.GetAllWards();
            if (wards == null)
            {
                return NoContent();
            }
            return Ok(wards);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetWard(int id)
        {
            var ward = await this.WardService.GetWard(id);

            if (ward == null)
            {
                return NotFound("The patient cannot be found");
            }
            return Ok(ward);
        }

        [HttpPost]
        public async Task<IActionResult> CreateWard([FromBody] Ward ward)
        {
            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateWard(int id, [FromBody] Ward ward)
        {
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteWard(int id)
        {
            return Ok();
        }
    }
}
