﻿using Domain;
using Microsoft.AspNetCore.Mvc;
using Service;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TestAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PatientController : ControllerBase
    {
        private readonly IPatientService PatientService;
        public PatientController(IPatientService patientService)
        {
            this.PatientService = patientService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllPatients()
        {
            var patients = await this.PatientService.GetAllPatients();

            if (!patients.Any())
            {
                return NoContent();
            }
            return this.Ok(patients);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetPatient(int id)
        {
            var patient = await this.PatientService.GetPatient(id);

            if (patient == null)
            {
                return NotFound("The patient cannot be found");
            }
            return Ok(patient);
        }

        [HttpPost]
        public async Task<IActionResult> CreatePatient([FromBody] Patient patient)
        {
            if (patient == null)
            {
                return BadRequest();
            }
            var patientToCreate = await this.PatientService.CreatePatient(patient); 
            return Ok(patientToCreate);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdatePatient([FromBody] Patient patient)
        {
            var patientToUpdate = await this.PatientService.GetPatient(patient.Id);

            if (patientToUpdate == null)
            {
                return NotFound();
            }
            var result = await this.PatientService.UpdatePatient(patientToUpdate);
            return Ok(result);
        }

        [HttpPut("{patientId}/ward/{wardId}")]
        public async Task<IActionResult> AssignAWard(int patientId, int wardId)
        {
            var result = await this.PatientService.AssignAWard(patientId, wardId);

            if (result == null)
            {
                return BadRequest("This patient is dead!");
            }
            return Ok(result);
        }

        [HttpPut("{id}/dateofdeath")]
        public async Task<IActionResult> UpdateDateOfDeath(int patientId, DateTime dateOfDeath)
        {
            var result = await this.PatientService.UpdateDateOfDeath(patientId, dateOfDeath);
            return Ok(result);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePatient(int id)
        {
            var patientToDelete = await this.PatientService.GetPatient(id);

            if (patientToDelete == null)
            {
                return NoContent();
            }
            var result = await this.PatientService.DeletePatient(id);
            return Ok(result);
        }

        [HttpPut("{patientId}/transferward/{wardId}")]
        public async Task<IActionResult> TransferWard(int patientId, int wardId)
        {
            var patientToTransfer = await this.PatientService.GetPatient(patientId);

            if (patientToTransfer == null)
            {
                return NotFound();
            }
            var result = await this.PatientService.TransferWard(patientId, wardId);
            return Ok(result);
        }
    }
}
