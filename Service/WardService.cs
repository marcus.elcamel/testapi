﻿using Domain;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class WardService : IWardService
    {
        private readonly IWardRepository WardRepository;
        public WardService(IWardRepository wardRepository)
        {
            this.WardRepository = wardRepository;
        }

        public async Task<Ward> CreateWard(Ward ward)
        {
            throw new NotImplementedException();
        }

        public async Task<int> DeleteWard(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<Ward>?> GetAllWards()
        {
            var wards = await this.WardRepository.GetAllWards();
            if (!wards.Any())
            {
                return null;
            }
            return wards;
        }

        public async Task<Ward> GetWard(int id)
        {
            var result = await this.WardRepository.GetWard(id);
            return result;
        }

        public async Task<int> UpdateWard(Ward ward)
        {
            throw new NotImplementedException();
        }
    }
}
