﻿using System;
using Domain;
using Infrastructure;

namespace Service
{
    public class PatientService : IPatientService
    {
        private readonly IPatientRepository PatientRepository;
        private readonly IWardRepository WardRepository;
        public PatientService(IPatientRepository patientRepository, IWardRepository wardRepository)
        {
            this.PatientRepository = patientRepository;
            this.WardRepository = wardRepository;
        }


        public async Task<Patient> CreatePatient(Patient patient)
        {
            if (string.IsNullOrEmpty(patient.Surname) ||
                string.IsNullOrEmpty(patient.Forename) ||
                string.IsNullOrEmpty(patient.Sex)
                )
            {
                throw new ArgumentException();
            }
            else if (patient.DateOfBirth == DateTime.MinValue)
            {
                throw new ArgumentException();
            }
            patient = await this.PatientRepository.CreatePatient(patient);
            return patient;
        }

        public async Task<int> DeletePatient(int id)
        {
            var result = await this.PatientRepository.DeletePatient(id);
            return result;
        }

        public async Task<List<Patient>> GetAllPatients()
        {
            var result = await this.PatientRepository.GetAllPatients();
            return result;
        }

        public async Task<Patient> GetPatient(int id)
        {
            var result = await this.PatientRepository.GetPatient(id);
            return result;
        }

        public async Task<int?> UpdatePatient(Patient patient)
        {
            var patientToUpdate = await this.PatientRepository.GetPatient(patient.Id);

            if (patientToUpdate == null)
            {
                return null;
            }
            var result = await this.PatientRepository.UpdatePatient(patientToUpdate);
            return result;
        }

        public async Task<int?> AssignAWard(int patientId, int wardId)
        {
            var patientToUpdate = await this.PatientRepository.GetPatient(patientId);

            if (patientToUpdate.DateOfDeath.HasValue)
            {
                return null;
            }
            var wardToAssign = await this.PatientRepository.AssignAWard(patientId, wardId);
            return wardToAssign;
        }

        public async Task<int?> UpdateDateOfDeath(int patientId, DateTime dateOfDeath)
        {
            var deceasedPatient = await this.PatientRepository.GetPatient(patientId);
            
            if (deceasedPatient == null)
            {
                return null;
            }
            var result = await this.PatientRepository.UpdateDateOfDeath(deceasedPatient.Id, dateOfDeath);
            return result;
        }

        public async Task<int?> TransferWard(int patientId, int wardId)
        {
            var patientToTransfer = await this.PatientRepository.GetPatient(patientId);
            var currentWard = await this.WardRepository.GetWard(patientToTransfer.WardId);
            var wardGender = await this.PatientRepository.GetWardGender(patientId);

            if (currentWard.MixedSex == false && patientToTransfer.Sex == wardGender)
            {
                var mixedResult = await this.PatientRepository.TransferWard(patientToTransfer.Id, patientToTransfer.WardId);
                return mixedResult;
            }
            return null;
        }

        public async Task<string> GetWardGender(int id)
        {
            var result = await this.PatientRepository.GetWardGender(id);
            return result;
        }
    }
}