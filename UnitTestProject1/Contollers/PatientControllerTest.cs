﻿/*using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Threading.Tasks;
using Xunit;
using Service;
using TestAPI.Controllers;

namespace TestApi
{
    public class PatientControllerTest
    {
        #region Get

        [Fact]
        public async Task Get_InvalidId_ReturnNotFound()
        {
            //Arrange 
            int id = default;

            var sut = this.GetSubjectUnderTest();

            //Act
            var response = await sut.GetPatient(id);

            //Assert
            Assert.NotNull(response);
            Assert.IsType<NotFoundResult>(response);

            await this.Repository.Received().GetPatient(id);
        }

        #endregion

        #region Setup

        private IPatientService Repository = Substitute.For<IPatientService>();
        private PatientController GetSubjectUnderTest()
        {
            return new PatientController(this.Repository);
        }

        #endregion

    }
}*/