﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Xunit;

namespace TestAPI2
{
    [TestClass]
    public class UnitTest1
    {
        #region Get

        [Fact]
        public async Task Get_InvalidId_ReturnNotFound()
        {
            //Arrange 
            int id = default;

            var sut = this.GetSubjectUnderTest();

            //Act
            var response = await sut.GetPatient(id);

            //Assert
            Assert.NotNull(response);
            Assert.IsType<NotFoundResult>(response);

            await this.Repository.Received().GetPatient(id);
        }

        #endregion

        #region Setup

        private IPatientService Repository = Substitute.For<IPatientService>();
        private PatientController GetSubjectUnderTest()
        {
            return new PatientController(this.Repository);
        }

        #endregion
    }
}
