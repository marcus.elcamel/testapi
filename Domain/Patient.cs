﻿namespace Domain
{
    public class Patient
    {
        public int Id { get; set; }
        public long NhsNumber { get; set; }
        public string Surname { get; set; } = String.Empty;
        public string Forename { get; set; } = String.Empty;
        public string Sex { get; set; } = String.Empty;
        public DateTime DateOfBirth { get; set; } 
        public DateTime? DateOfDeath { get; set; }
        public DateTime? DateOfDischarge { get; set; }
        public int WardId { get; set; }
    }
}   