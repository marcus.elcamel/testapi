﻿using Dapper;
using Domain;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Infrastructure
{
    public class WardRepository : IWardRepository
    {
        private IDbConnection dbConnection;
        public WardRepository(IConfiguration configuration)
        {
            this.dbConnection = new SqlConnection(configuration.GetConnectionString("DefaultConnection"));
        }

        public async Task<Ward> CreateWard(Ward ward)
        {
            throw new NotImplementedException();
        }

        public async Task<int> DeleteWard(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<Ward>?> GetAllWards()
        {
            var sql = @"SELECT *
                        FROM Ward";

            var result = await this.dbConnection.QueryAsync<Ward>(sql);
            return result.ToList();
        }

        public async Task<Ward> GetWard(int id)
        {
            var sql = @"SELECT *
                        FROM Ward 
                        WHERE Id = @Id";

            var parameters = new { Id = id };
            var result = await this.dbConnection.QuerySingleAsync<Ward>(sql, parameters);
            return result;
        }

        public async Task<int> UpdateWard(Ward ward)
        {
            throw new NotImplementedException();
        }
    }
}