﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public interface IPatientRepository
    {
        Task<Patient> CreatePatient(Patient patient);
        Task<Patient> GetPatient(int id);
        Task<int> DeletePatient(int id);
        Task<List<Patient>> GetAllPatients();
        Task<int?> UpdatePatient(Patient patient); // dont mind this
        Task<int?> AssignAWard(int patientId, int wardId);
        Task<int?> UpdateDateOfDeath(int patientId, DateTime dateOfDeath);
        Task<int?> TransferWard(int patientId, int wardId);
        Task<string> GetWardGender(int id);
    }
}
