﻿using Dapper;
using Domain;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Infrastructure
{
    public class PatientRepository : IPatientRepository
    {
        private IDbConnection dbConnection;
        public PatientRepository(IConfiguration configuration)
        {
            this.dbConnection = new SqlConnection(configuration.GetConnectionString("DefaultConnection"));
        }

        public async Task<Patient> CreatePatient(Patient patient)
        {
            var sql = @"INSERT INTO Patient (NhsNumber, Surname, Forename, Sex, DateOfBirth, DateOfDeath, DateOfDischarge, WardId)
                        OUTPUT inserted.ID
                        VALUES (@NhsNumber, @Surname, @Forename, @Sex, @DateOfBirth, @DateOfDeath, @DateOfDischarge, @WardId)";

            var parameters = new
            {
                NhsNumber = patient.NhsNumber,
                Surname = patient.Surname,
                Forename = patient.Forename,
                Sex = patient.Sex,
                DateOfBirth = patient.DateOfBirth,
                DateOfDeath = (DateTime?) null,
                DateOfDischarge = (DateTime?) null,
                WardId = 1
            };
            var id = await this.dbConnection.QuerySingleAsync<int>(sql, parameters);
            patient.Id = id;

            return patient;
        }

        public async Task<int> DeletePatient(int id)
        {
            var sql = @"DELETE 
                        FROM Patient
                        WHERE Patient.Id = @Id";

            var parameters = new { Id = id };

            var affectedRows = await this.dbConnection.ExecuteAsync(sql, parameters);
            return affectedRows;
        }

        public async Task<List<Patient>> GetAllPatients()
        {
            var sql = @"SELECT * 
                        FROM Patient";

            var result = await this.dbConnection.QueryAsync<Patient>(sql);
            return result.ToList();
        }

        public async Task<Patient> GetPatient(int id)
        {
            var sql = @"SELECT *
                        FROM Patient 
                        WHERE Id = @Id";

            var parameters = new { Id = id };
            var result = await this.dbConnection.QuerySingleAsync<Patient>(sql, parameters);
            return result;
        }

        public async Task<int?> UpdatePatient(Patient patient)
        {
            var sql = @"UPDATE Patient
                        SET NhsNumber = @NhsNumber,
	                        Surname = @Surname,
	                        Forename = @Forename,
	                        Sex = @Sex,
	                        DateOfBirth = @DateOfBirth,
                            DateOfDeath = @DateOfDeath,
	                        DateOfDischarge = @DateOfDischarge,
	                        WardId = @WardId
	                    WHERE Id = @Id";
                            
            var parameters = new
            {
                Id = patient.Id,
                NhsNumber = patient.NhsNumber,
                Surname = patient.Surname,
                Forename = patient.Forename,
                Sex = patient.Sex,
                DateOfBirth = patient.DateOfBirth,
                DateOfDeath = patient.DateOfDeath,
                DateOfDischarge = patient.DateOfDischarge,
                WardId = patient.WardId
            };
            var result = await this.dbConnection.ExecuteAsync(sql, parameters);
            return result;
        }

        public async Task<int?> AssignAWard(int patientId, int wardId)
        {
            var sql = @"UPDATE Patient
                        SET WardId = @WardId
                        WHERE Patient.Id = @PatientId";

            var parameters = new { PatientId = patientId, WardId = wardId };
            var result = await this.dbConnection.ExecuteAsync(sql, parameters);
            return result;
        }

        public async Task<int?> UpdateDateOfDeath(int patientId, DateTime dateOfDeath)
        {
            var deceasedPatient = await this.GetPatient(patientId);

            var sql = @"UPDATE Patient
                        SET DateOfDeath = @DateOfDeath,
                            WardId = @WardId
                        WHERE Patient.Id = @PatientId";

            var parameters = new { PatientId = patientId, DateOfDeath = dateOfDeath, WardId = (int?) null};
            var result = await this.dbConnection.ExecuteAsync(sql, parameters);
            return result;
        }

        public async Task<int?> TransferWard(int patientId, int wardId)
        {
            var sql = @"UPDATE Patient
                        SET WardId = @WardId
                        WHERE Patient.Id = @PatientId";

            var parameters = new { PatientId = patientId, WardId = wardId };
            var result = await this.dbConnection.ExecuteAsync(sql, parameters);
            return result;
        }
        public async Task<string> GetWardGender(int id)
        {
            var sql = @"SELECT Sex
                        FROM Patient
                        WHERE Id = @Id";

            var parameters = new { Id = id };
            var result = await this.dbConnection.QuerySingleAsync<string>(sql, parameters);
            return result;
        }
    }
}