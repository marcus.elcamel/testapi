﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public interface IWardRepository
    {
        Task<Ward> CreateWard(Ward ward);
        Task<int> UpdateWard(Ward ward);
        Task<Ward> GetWard(int id);
        Task<int> DeleteWard(int id);
        Task<List<Ward>?> GetAllWards();
    }
}
